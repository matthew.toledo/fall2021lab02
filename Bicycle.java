// Matthew Toledo
// 2033916

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
            this.manufacturer = manufacturer;
            this.numberGears = numberGears;
            this.maxSpeed = maxSpeed;
    }

    public String toString() {
        String s = "Manufacturer: " + this.manufacturer + ", Number of gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
        return s;
    } 

}